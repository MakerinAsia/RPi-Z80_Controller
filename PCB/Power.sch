EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title "Raspberry Pi - Z80 Controller - Z80"
Date "2021-03-29"
Rev "1.1"
Comp "RPL"
Comment1 "Based on:  https://www.hackster.io/james-fitzjohn/raspberry-pi-to-z80-interface-0bfbeb"
Comment2 "https://gitlab.com/MakerinAsia/RPi-Z80_Controller/"
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 4940 3945 0    50   ~ 0
8V - 17V DC-DC Buck converter\n(SY8104ADC IC) to 5V at 3A to 4A could go here\n2.1mm SMD Barrel Jack input
$EndSCHEMATC
